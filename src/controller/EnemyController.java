package controller;

import model.Map;
import model.MetaSettings;

/**
 * Created by Mathis on 24/11/15.
 */
public class EnemyController implements Runnable {

    private Map map;

    public EnemyController(Map map) {
        this.map = map;
    }

    @Override
    public void run() {
        while(GameController.gameIsRunning) {
            //TODO: All enemies move.
            try {
                Thread.sleep(MetaSettings.TICK);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
