package controller;

import model.Map;

/**
 * Created by Mathis on 24/11/15.
 */
public class GameController {

    static boolean gameIsRunning = false;

    private Map map = new Map();
    private Thread enemyThread, towerThread;

    public GameController() {
        enemyThread = new Thread(new EnemyController(map));
        towerThread = new Thread(new TowerController(map));
    }


}
