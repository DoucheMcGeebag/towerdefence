package model.test;

import model.entities.Enemy;
import model.entities.Tower;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Mathis on 07/12/15.
 */
public class TowerTest {

    Tower tower;

    @Before
    public void setUp() throws Exception {
        this.tower = new Tower(0, 0, 10, 10);
    }

    @After
    public void tearDown() throws Exception {
        this.tower = null;
    }

    @Test
    public void testUpgrade() throws Exception {
        assertEquals(1, tower.getTowerLevel());
        for (int i = 2; i < 5; i++) {
            tower.upgrade();
            assertEquals(i, tower.getTowerLevel());
        }
    }

    @Test
    public void testMaxUpgrade() throws Exception {
        assertEquals(1, tower.getTowerLevel());
        for (int i = 0; i < 5; i++) {
            tower.upgrade();
        }
        assertEquals(5, tower.getTowerLevel());
    }

    @Test
    public void testShoot() throws Exception {
        for (int i = 0; i < 5; i++) {
            assertEquals(tower.getBaseDamage() * tower.getTowerLevel(), tower.shoot());
            tower.upgrade();
        }
    }

    @Test
    public void testStandby() throws Exception {
        int tempCooldown = tower.getCooldown();
        tower.standby();
        if(tempCooldown > 0) {
            assertEquals(true, tempCooldown > tower.getCooldown());
        }
        else if (tempCooldown == 0) {
            assertEquals(true, tempCooldown == tower.getCooldown());
        }
    }

    @Test
    public void testUpdate() throws Exception {
        Enemy enemy = new Enemy(1);
        tower.update(enemy);

        assertEquals(enemy, tower.getTarget());
    }

    @Test
    public void testUpdate1() throws Exception {
        Enemy enemy = new Enemy(1);
        tower.update(enemy);

        int initialEnemyHealth = enemy.getHealth();

        //simulating game loop (cooldown cycle)
        for (int i = 0; i < 11; i++) {
            tower.update();
        }

        assertEquals(true, enemy.getHealth() < initialEnemyHealth);
    }
}