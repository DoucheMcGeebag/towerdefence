package model;

import model.entities.Enemy;

import java.util.ArrayList;

/**
 * Created by Mathis, Simon, Tobias on 23/11/15.
 */
public class Level {

    private int enemyCount = 10;
    private ArrayList<Enemy> enemyList = new ArrayList<>();

    public Level(int levelNumber){
        for (int i = 0; i < enemyCount; i++) {
            enemyList.add(new Enemy(levelNumber));
        }
    }

    //Method that the Map should call after the level constructor, to get Enemies list
    public ArrayList<Enemy> getEnemyList() {
        return enemyList;
    }

}
