package model.entities;

/**
 * Created by Mathis, Simon, Tobias on 23/11/15.
 */
public class Tower {

    private int  price,
            baseDamage,
            cooldown,    //1 tick unit
            attackSpeed, //1 tick unit
            damageType,
            towerLevel,
            range,
            positionX,
            positionY;

    private Enemy target;


    public void upgrade(){
        if(towerLevel<5)
            towerLevel++;
    }

    public int shoot(){
        cooldown = attackSpeed;
        return baseDamage*towerLevel;
    }

    public void standby(){
        if (cooldown > 0) {
            cooldown--;
        }
    }

    public Tower(int posX, int posY, int baseDamage, int attackSpeed){
        this.positionX = posX;
        this.positionY = posY;
        this.towerLevel = 1;
        this.baseDamage = baseDamage;
        this.attackSpeed = attackSpeed;
        this.cooldown = attackSpeed;
    }

    //update with new target, when tower doesn't already have valid target. Miss turn to shoot once.
    public void update(Enemy newTarget) {

        target = newTarget;
    }

    //update with no new target, since tower already has a valid target. (SHOOT!... maybe)
    public void update() {
        if(cooldown <= 0){
            target.takeDamage(shoot());
        }
        else{
            standby();
        }
    }

    public Enemy getTarget() {
        return target;
    }

    public int getRange() {
        return range;
    }

    public int getPositionX() {
        return positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    public int getTowerLevel() {
        return towerLevel;
    }

    public int getBaseDamage() {
        return baseDamage;
    }

    public int getCooldown() {
        return cooldown;
    }
}
