package model.entities;

/**
 * Created by Mathis, Simon, Tobias on 23/11/15.
 */
public class Enemy {


    private int  type,
            health,
            initSpeed,
            currentSpeed,
            positionX,
            positionY,
            previousTileDirection;
    private boolean isDead;


    public Enemy(int levelNumber){
        //after loop type will always be between 1-4
        this.type = levelNumber;
        while(type>4){
            type-=4;
        }

        this.health = 100 +10*levelNumber;
        this.currentSpeed = 1;
        this.isDead = false;
    }

    public void move ( int[] surroundings){

        int direction = -1;

        for (int i = 0; i < surroundings.length; i++) {
            if(surroundings[i] == 0 && i != previousTileDirection){
                direction = i;
                break;
            }
        }

        switch (direction){
            case 0: //move North
                positionY -= currentSpeed;
                previousTileDirection = 1;
                break;
            case 1: //move South
                positionY += currentSpeed;
                previousTileDirection = 0;
                break;
            case 2: //move East
                positionX += currentSpeed;
                previousTileDirection = 3;
                break;
            case 3: //move West
                positionX -= currentSpeed;
                previousTileDirection = 2;
                break;
            default:
                System.err.println("Invalid direction given.");
                break;
        }
    }

    //Take damage
    public void takeDamage(int damage){
        this.health -= damage;
        if(health <= 0){
            this.isDead = true;
        }
    }

    public boolean isDead(){
        return isDead;
    }

    public int getPositionX() {
        return positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    //TODO: finish slow
    //amount 0-100 where 100 is 100% slowed.
    public void slow(int amount, int duration){
        currentSpeed = (currentSpeed * (100 - amount)) / 100;
    }

    public int getPreviousTileDirection() {
        return previousTileDirection;
    }

    public int getHealth() {
        return health;
    }
}
