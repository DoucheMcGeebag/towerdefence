package model;

import model.entities.Enemy;
import model.entities.Tower;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Mathis on 24/11/15.
 */
public class Map {

    private Level level;
    private int currentLevel = 0;
    private int[][] mapGrid;
    private ArrayList<Tower> towerList;
    private ArrayList<Enemy> enemyList;
    private int tileSize = 40;

    //1 = BUILDABLE AREA
    //0 = ENEMY PATH
    //2 = OCCUPIED BY TOWER
    //3 = ENEMY STARTING POINT
    //4 = ENEMY ENDING POINT
    //9 = MAP BORDER (AKA. GREAT WALL OF CHINA)
    private final int[] defaultMap = {
            9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,
            9,1,3,1,1,1,1,1,1,1,1,1,1,1,1,9,
            9,1,0,1,1,1,1,1,1,1,1,1,1,1,1,9,
            9,1,0,0,0,0,0,0,0,0,0,0,0,0,1,9,
            9,1,1,1,1,1,1,1,1,1,1,1,1,0,1,9,
            9,1,1,1,1,1,1,1,1,1,1,1,1,0,1,9,
            9,1,0,0,0,0,0,0,0,0,0,0,0,0,1,9,
            9,1,0,1,1,1,1,1,1,1,1,1,1,1,1,9,
            9,1,0,1,1,1,1,1,1,1,1,1,1,1,1,9,
            9,1,0,0,0,0,0,0,0,0,0,0,0,0,1,9,
            9,1,1,1,1,1,1,1,1,1,1,1,1,0,1,9,
            9,1,1,1,1,1,1,1,1,1,1,1,1,0,1,9,
            9,1,0,0,0,0,0,0,0,0,0,0,0,0,1,9,
            9,1,0,1,1,1,1,1,1,1,1,1,1,1,1,9,
            9,1,4,1,1,1,1,1,1,1,1,1,1,1,1,9,
            9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9
    };


    public Map() {
        this.nextLevel();
        this.generateMapGrid(defaultMap, 16, 16);
        this.towerList = new ArrayList<>();
    }

    public void generateMapGrid(int[] map, int sizeX, int sizeY) {
        this.mapGrid = new int[sizeX][sizeY];

        for (int y = 0; y < sizeY; y++) {
            for (int x = 0; x < sizeX; x++) {
                mapGrid[x][y] = map[x+(y*sizeX)];
            }
        }
    }

    public void updateTowers () {
        for (Tower tower : towerList) {

            //if no target OR target is dead OR target is out of range
            if(tower.getTarget() == null || tower.getTarget().isDead() || !inRange(tower, tower.getTarget())){

                for (Enemy enemy : enemyList) {
                    //is tower in range of enemy
                    if(inRange(tower, enemy)){
                        tower.update(enemy); //tell tower to do stuff with new target
                        break;
                    }
                }
            }
            else{ //Already has valid target
                tower.update(); //tell tower to do stuff with no target
            }
        }
    }

    public void updateEnemies () {
        /*TODO: Loop each enemy in array.
                Check if enemy is dead, if dead remove it from array.
                if not, enemy should choose direction to move.
                move in chosen direction.
                if the tile it moved to is goal, kill enemy, and decrement player health.
                After foreach loop, check if array is empty (level finished).
                then call next level.
        */

        Iterator<Enemy> enemyIterator = enemyList.iterator();

        while (enemyIterator.hasNext()) {
            Enemy enemy = enemyIterator.next();

            if (enemy.isDead()) {
                enemyIterator.remove();
            }
            else {

                int x = enemy.getPositionX();
                int y = enemy.getPositionY();

                int north = mapGrid[x][y-1];
                int south = mapGrid[x][y+1];
                int east = mapGrid[x+1][y];
                int west = mapGrid[x-1][y];

                int[] surroundings = {north, south, east, west};

                enemy.move(surroundings);
            }
        }
    }

    private void nextLevel () {
        currentLevel++;
        level = new Level(currentLevel);
        enemyList = level.getEnemyList();


    }

    private boolean inRange(Tower tower, Enemy enemy){

        int deltaX = tower.getPositionX() - enemy.getPositionX();
        int deltaY = tower.getPositionY() - enemy.getPositionY();
        int distance = (int) Math.sqrt(deltaX^2 + deltaY^2);

        if(tower.getRange() >= distance){
            return true;
        }

        return false;
    }


}
