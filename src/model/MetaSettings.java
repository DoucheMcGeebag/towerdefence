package model;

/**
 * Created by Mathis, Simon, Tobias on 23/11/15.
 */
public class MetaSettings {
    public static final int TICK = 30; //meaning that a tick is 30 ms
    public static final int TILESIZE = 40; //pixel size of one tile
}
