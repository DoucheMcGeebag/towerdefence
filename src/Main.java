import model.entities.Tower;

import java.util.ArrayList;
import java.util.Iterator;

public class Main {

    public static void main(String[] args) {


        ArrayList<String> enemyList = new ArrayList<>();

        enemyList.add("hej");
        enemyList.add("abc");
        enemyList.add("abc");
        enemyList.add("hej");
        enemyList.add("abc");
        enemyList.add("xxx");

        for (String s : enemyList) {
            System.out.println(s);
        }
        System.out.println(enemyList.size());

        Iterator<String> enemyIterator = enemyList.iterator();

        while (enemyIterator.hasNext()) {
            String enemy = enemyIterator.next();

            if (enemy.equals("hej")) {
                enemyIterator.remove();
                System.out.println(enemyList.size());
            }
        }

        System.out.println();

        for (String s : enemyList) {
            System.out.println(s);
        }
        System.out.println(enemyList.size());

    }
}
